package w.it.aurora.http;

import lombok.NonNull;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public final class HttpParameters {

    private final String url;
    private final Map<String, String> parameters;

    private HttpParameters(@NonNull String url) {
        this.url = url;
        this.parameters = new ConcurrentHashMap<>();
    }

    public static HttpParameters create(@NonNull String url) {
        return new HttpParameters(url);
    }

    private String encode(String value) {
        return URLEncoder.encode(value, StandardCharsets.UTF_8);
    }

    public HttpParameters addParameter(@NonNull String key, @NonNull Object value) {
        parameters.put(key, value.toString());
        return this;
    }

    public String appendParameters() {
        return (url + (url.contains("?") ? "" : "?")) + parameters.entrySet()
                .stream()

                .map(entry -> entry.getKey() + "=" + (entry.getValue() != null ? encode(entry.getValue()) : null))
                .collect(Collectors.joining("&"));
    }
}
