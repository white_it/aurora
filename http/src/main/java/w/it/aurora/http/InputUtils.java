package w.it.aurora.http;

import lombok.NonNull;
import lombok.experimental.UtilityClass;

import java.io.InputStream;

@UtilityClass
public class InputUtils {

    public boolean isEmpty(@NonNull InputStream inputStream) {
        try {
            return inputStream.available() <= 0;
        } catch (Exception ex) {
            return true;
        }
    }

    public byte[] toByteArray(@NonNull InputStream inputStream) {
        try {
            byte[] result = new byte[inputStream.available()];
            inputStream.read(result);

            return result;
        } catch (Exception ex) {
            return new byte[0];
        }
    }
}
