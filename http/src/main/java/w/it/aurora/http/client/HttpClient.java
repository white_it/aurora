package w.it.aurora.http.client;

import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import lombok.NonNull;
import lombok.val;
import w.it.aurora.http.InputUtils;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;

public final class HttpClient {

    static final Gson GSON = new Gson();

    private final HttpClientConfiguration config = new HttpClientConfiguration();

    private final Logger logger = Logger.getLogger("HttpClient");

    public Logger logger() {
        return logger;
    }

    public HttpClientConfiguration config() {
        return config;
    }

    public HttpRequest newRequest(String url) {
        return new HttpRequest(this, url);
    }

    public HttpRequest newRequest(HttpRequest.Type type, String url) {
        return new HttpRequest(this, type, url);
    }

    HttpResponse execute(@NonNull HttpRequest request) {
        String url = request.getUrl();
        Preconditions.checkState(url != null, "URL is null, the action is cancelled.");

        try {
            HttpURLConnection http = ((HttpURLConnection) new URL(url).openConnection());

            http.setConnectTimeout(config.getConnectTimeout());
            http.setReadTimeout(config.getReadTimeout());
            http.setRequestMethod(request.getType().toString());

            config.getHeaders().forEach(http::setRequestProperty);

            if (request.getContentAsByteArray().length > 0) {
                http.setDoOutput(true);

                try (val outputStream = http.getOutputStream()) {

                    outputStream.write(request.getContentAsByteArray());
                    outputStream.flush();
                }
            }

            http.setDoInput(true);

            try (InputStream inputStream = http.getInputStream()) {

                byte[] callbackArray = InputUtils.toByteArray(inputStream);
                String callback = new String(callbackArray, 0, callbackArray.length, StandardCharsets.UTF_8);

                HttpResponse httpResponse = HttpResponse.create(http.getContentLength(), http.getResponseCode(), callback);

                http.disconnect();
                return httpResponse;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
