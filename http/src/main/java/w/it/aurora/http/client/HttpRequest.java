package w.it.aurora.http.client;

import lombok.Getter;
import lombok.NonNull;
import w.it.aurora.http.HttpParameters;
import w.it.aurora.http.HttpUtils;
import w.it.aurora.http.InputUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

@Getter
public class HttpRequest {

    private static final ExecutorService THREAD_POOL = Executors.newCachedThreadPool();
    private final HttpClient client;
    private final Type type;
    private final HttpParameters httpParameters;
    private final ByteArrayOutputStream content;
    private String url;
    private final String path;

    HttpRequest(@NonNull HttpClient client, @NonNull Type type, @NonNull String url) {
        this.client = client;
        this.type = type;
        this.url = url;
        this.path = null;

        this.httpParameters = HttpParameters.create(url);
        this.content = new ByteArrayOutputStream();
    }

    HttpRequest(@NonNull HttpClient client, @NonNull String url) {
        this(client, Type.GET, url);
    }

    public byte[] getContentAsByteArray() {
        return content.toByteArray();
    }

    public HttpRequest appendProtocolToUrl() {
        url = HttpUtils.getProtocol(url) + "://" + HttpUtils.trimProtocol(url);
        return this;
    }

    public HttpRequest setPath(@NonNull String path) {
        url = this.url.concat(path);
        return this;
    }

    public HttpRequest modifyUrlParams(Consumer<HttpParameters> parametersConsumer) {
        if (parametersConsumer != null) {
            parametersConsumer.accept(httpParameters);

            this.url = httpParameters.appendParameters();
        }

        return this;
    }

    public HttpRequest addParamToUrl(String key, Object value) {
        return modifyUrlParams(params -> params.addParameter(key, value));
    }

    public HttpRequest setContent(byte[] contentAsBytes) {
        try {
            content.write(contentAsBytes);
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        return this;
    }

    public HttpRequest setContent(String contentMessage, Charset charset) {
        return setContent(contentMessage.getBytes(charset));
    }

    public HttpRequest setContent(String contentMessage) {
        return setContent(contentMessage, StandardCharsets.UTF_8);
    }

    public HttpRequest setContentByFile(@NonNull File file) {
        try (FileInputStream inputStream = new FileInputStream(file)) {
            setContent(InputUtils.toByteArray(inputStream));
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        return this;
    }

    public HttpRequest setContentJson(Object object) {
        return setContent(HttpClient.GSON.toJson(object));
    }

    private HttpResponse execute() {
        return client.execute(this);
    }

    public final CompletableFuture<HttpResponse> executeSync() {
        return CompletableFuture.completedFuture(execute());
    }

    public final CompletableFuture<HttpResponse> executeAsync() {
        return CompletableFuture.supplyAsync(this::execute, THREAD_POOL);
    }


    public enum Type {
        GET, HEAD, POST, PUT, DELETE, CONNECT, OPTIONS, TRACE, PATCH
    }
}
