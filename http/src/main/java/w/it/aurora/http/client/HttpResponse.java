package w.it.aurora.http.client;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.nio.charset.Charset;

@Getter
@RequiredArgsConstructor
public final class HttpResponse {

    private final int contentLength;
    private final int statusCode;
    private final String body;

    public static HttpResponse create(int contentLength, int statusCode, String callback) {
        return new HttpResponse(contentLength, statusCode, callback);
    }

    public <T> T getBodyFromJson(Class<T> type) {
        return HttpClient.GSON.fromJson(body, type);
    }

    public byte[] getBody() {
        return body.getBytes();
    }

    public String getBodyAsString() {
        return body;
    }

    public byte[] getBody(Charset charset) {
        return body.getBytes(charset);
    }
}
