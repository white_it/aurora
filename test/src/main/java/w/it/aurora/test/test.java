package w.it.aurora.test;

import w.it.aurora.test.gitlab.GitLabUtils;

public class test {

    public static void main(String[] args) {
        GitLabUtils.readFile("localization", "languages.json").whenComplete((httpResponse, throwable) -> {
            GitLabUtils.HTTP_CLIENT.logger().info(httpResponse.getBodyAsString());
        }).join();
    }
}
