package w.it.aurora.test.util;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder(builderMethodName = "newBuilder", buildMethodName = "compile")
public class Pair<A, B> {

    private final A first;
    private final B second;
}
